# Modules

- [DirtySubmit](dirty_submit.md)

  Disable form submits until there are unsaved changes.

- [Merge Request widget extensions](widget_extensions.md)

  Easily add extensions into the merge request widget
